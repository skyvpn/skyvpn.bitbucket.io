<div class="section overlay-shape footer-section footer-salvia section-pad-sm">
	<div class="container">
		<div class="footer-bottom">
			<ul class="footer-list text-center cursor-default">
				<li>
					<div>
						<h4>SKYVPN</h4>
						<ul class="display-block text-left">
							<li><a href="/">Home</a></li>
						
							<li><a href="support">Support</a></li>
							<li><a href="signup">Sign UP</a></li>
						</ul>
					</div>
				</li>
				<li>
					<div>
						<ul class="display-block">
							<li>
								<a class="btn" href="/download/setup.exe">Download</a>
							</li>
						</ul>
					</div>
				</li>
			</ul>
			<ul class="footer-list text-center cursor-default">
				<li>Copyright © 2017 — 2020 SKYVPN. All Right Reserved</li>
			</ul>
		</div>
	</div><!-- .container -->
	<a href="#" id="toTopButton" class="animatebtn"><i class="fas fa-chevron-up"></i></a>
	<script>
		scrolltop();
		topFunction();
	</script>
</div>