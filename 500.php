<!DOCTYPE html>
<html lang="en" class="js">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>SAMPLEVPN</title>
        <!--Favicon add-->
        <link rel="shortcut icon" type="image/png" href="/assets/images/logo/icon.png">
        <!--Style Css-->
        <link href="css/second.css" rel="stylesheet">
        <link href="css/errors.css" rel="stylesheet">

        <link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/sweetalert/1.1.3/sweetalert.min.css">
        <link href="/assets/front/css/vendor.bundle.css" rel="stylesheet">
        <link href="https://fonts.googleapis.com/css?family=Roboto:100,100italic,300,300italic,400,400italic,500,500italic,700,700italic,900,900italic" rel="stylesheet" type="text/css" />
        <!--Font Awasome-->
        <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.4.2/css/all.css" integrity="sha384-/rXc/GQVaYpyDdyxK+ecHPVYJSN9bmVFBvjA/9eOB+pb3F2w2N6fc5qB9Ew5yIns" crossorigin="anonymous">
        <link rel="stylesheet" href="../assets/front/css/FontAwasomeCSS/font-awesome.css">
        <!--SCRIPTS-->

        <script src="/assets/admin/global/plugins/jquery.min.js" type="text/javascript"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/sweetalert/1.1.3/sweetalert.min.js"></script>
        <script type="text/javascript" src="/assets/user/js/functions.js"></script>
        <script type="text/javascript" src="/assets/front/js/script.js"></script>
    </head>
    <body>
    <div class="moon"></div>
<div class="moon__crater moon__crater1"></div>
<div class="moon__crater moon__crater2"></div>
<div class="moon__crater moon__crater3"></div>

<div class="star star1"></div>
<div class="star star2"></div>
<div class="star star3"></div>
<div class="star star4"></div>
<div class="star star5"></div>

<div class="error">
  <div class="error__title">500</div>
  <div class="error__subtitle">Hmmm...</div>
  <div class="error__description">It looks like one of the  developers fell asleep</div>
  <a class="error__button error__button--active" href="/">HOME</a>
  <a class="error__button" href="signup">Sign UP</a>
</div>

<div class="astronaut">
  <div class="astronaut__backpack"></div>
  <div class="astronaut__body"></div>
  <div class="astronaut__body__chest"></div>
  <div class="astronaut__arm-left1"></div>
  <div class="astronaut__arm-left2"></div>
  <div class="astronaut__arm-right1"></div>
  <div class="astronaut__arm-right2"></div>
  <div class="astronaut__arm-thumb-left"></div>
  <div class="astronaut__arm-thumb-right"></div>
  <div class="astronaut__leg-left"></div>
  <div class="astronaut__leg-right"></div>
  <div class="astronaut__foot-left"></div>
  <div class="astronaut__foot-right"></div>
  <div class="astronaut__wrist-left"></div>
  <div class="astronaut__wrist-right"></div>
  
  <div class="astronaut__cord">
    <canvas id="cord" height="500px" width="500px"></canvas>
  </div>
  
  <div class="astronaut__head">
    <canvas id="visor" width="60px" height="60px"></canvas>
    <div class="astronaut__head-visor-flare1"></div>
    <div class="astronaut__head-visor-flare2"></div>
  </div>
</div>
    <script>
        function drawVisor() {
  const canvas = document.getElementById('visor');
  const ctx = canvas.getContext('2d');
  
  ctx.beginPath();
  ctx.moveTo(5, 45);
  ctx.bezierCurveTo(15, 64, 45, 64, 55, 45);
  
  ctx.lineTo(55, 20);
  ctx.bezierCurveTo(55, 15, 50, 10, 45, 10);
  
  ctx.lineTo(15, 10);
  
  ctx.bezierCurveTo(15, 10, 5, 10, 5, 20);
  ctx.lineTo(5, 45);
  
  ctx.fillStyle = '#2f3640';
  ctx.strokeStyle = '#f5f6fa';
  ctx.fill();
  ctx.stroke();
}

const cordCanvas = document.getElementById('cord');
const ctx = cordCanvas.getContext('2d');

let y1 = 160;
let y2 = 100;
let y3 = 100;

let y1Forward = true;
let y2Forward = false;
let y3Forward = true;

function animate() {
  requestAnimationFrame(animate);
  ctx.clearRect(0, 0, innerWidth, innerHeight);
  
  ctx.beginPath();
  ctx.moveTo(130, 170);
  ctx.bezierCurveTo(250, y1, 345, y2, 400, y3);
  
  ctx.strokeStyle = 'white';
  ctx.lineWidth = 8;
  ctx.stroke();
 
  
  if (y1 === 100) {
    y1Forward = true;
  }
  
  if (y1 === 300) {
    y1Forward = false;
  }
  
  if (y2 === 100) {
    y2Forward = true;
  }
  
  if (y2 === 310) {
    y2Forward = false;
  }
  
  if (y3 === 100) {
    y3Forward = true;
  }
  
  if (y3 === 317) {
    y3Forward = false;
  }
  
  y1Forward ? y1 += 1 : y1 -= 1;
  y2Forward ? y2 += 1 : y2 -= 1;
  y3Forward ? y3 += 1 : y3 -= 1;
}

drawVisor();
animate();
    </script>
    </body>
</html>
