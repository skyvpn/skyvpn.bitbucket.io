<!DOCTYPE html>
<html lang="en" class="js">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="description" content="SKYVPN Best VPN for any devices">
        <meta name="keywords" content="SKYVPN, best vpn, vpn service, fastest vpn, anonymus service, anonymus vpn, bypass browser lock, bypass site lock, hide ip, change ip,">
        <title>SKYVPN</title>
        <!--Favicon add-->
        <link rel="shortcut icon" type="image/png" href="/assets/images/logo/icon.png">
        <!--Style Css-->
        <!--<link href="/css/style.css" rel="stylesheet">-->
        <link href="css/second.css" rel="stylesheet">
        
        <link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/sweetalert/1.1.3/sweetalert.min.css">
        <link href="/assets/front/css/vendor.bundle.css" rel="stylesheet">
        <link href="https://fonts.googleapis.com/css?family=Roboto:100,100italic,300,300italic,400,400italic,500,500italic,700,700italic,900,900italic" rel="stylesheet" type="text/css" />
        <!--Font Awasome-->
        <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.4.2/css/all.css" integrity="sha384-/rXc/GQVaYpyDdyxK+ecHPVYJSN9bmVFBvjA/9eOB+pb3F2w2N6fc5qB9Ew5yIns" crossorigin="anonymous">
        <link rel="stylesheet" href="../assets/front/css/FontAwasomeCSS/font-awesome.css">
        <!--SCRIPTS-->

        <script src="/assets/admin/global/plugins/jquery.min.js" type="text/javascript"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/sweetalert/1.1.3/sweetalert.min.js"></script>
        <script type="text/javascript" src="/assets/user/js/functions.js"></script>
        <script type="text/javascript" src="/assets/front/js/script.js"></script>
    </head>
    <body>
    <header class="site-header">
        <!--navbar area start-->
        <div class="navbar navbar-expand-lg is-transparent" id="mainnav">
			<nav class="container">
				<a class="navbar-brand animated" data-animate="fadeInDown" data-delay=".65" href="/">
					<img alt="logo" class="logo" src="/assets/front/images/logo.png">
				</a>
				<div class="collapse navbar-collapse justify-content-end" id="navbarToggle">
					<ul class="navbar-nav animated remove-animation" data-animate="fadeInDown" data-delay=".75">
						<li class="nav-item">
							<a class="nav-link menu-link" href="features">Features</a>
						</li>
						<li class="nav-item">
							<a class="nav-link menu-link" href="support">Support</a>
						</li>
						<li class="nav-item download-button">
							<a class="nav-link menu-link btn" href="/download/setup.exe">Download</a>
						</li>
					</ul>
				</div>
			</nav>
		</div>
    </header>

<div class="section section-bg top-300" id="overview">
	<div class="container">

        <div class="row align-items-center mb-100">
			
			<div class="col-lg-6">
                <div class="text-block">
                    <h2 class="section-title animated mb-4" data-animate="fadeInUp" data-delay=".2" >How can we help You ?</h2>
					<form class="support-form" action="">
                        <input type="email" name="" id="" placeholder="E-main">
                        <textarea name="" id="" cols="30" rows="10" placeholder="Type and click Submit"></textarea>
                        <button type="submit">Submit</button>
                    </form>
				</div>
            </div>
            
            <div class="col-lg-6">
				<div class="graph-img res-m-btm animated" data-animate="fadeInUp"
				data-delay=".1"><img alt="graph" src="/assets/front/images/salvia/c83e69dddcd624003936adef77ab7146cb7f45c8.svg"></div>
			</div>
        </div>
	</div>
</div>
<div class="section section-pad faq-section section-bg-alt" id="faq">
		<div class="container">
			<div class="row justify-content-center mb-5">
				<div class="col-lg-6">
					<div class="section-head-s5 text-center">
                        <h1 class="section-title animated fadeInUp" data-animate="fadeInUp" data-delay=".1" style="visibility: visible; animation-delay: 0s;">F.A.Q.</h1>
					</div>
				</div>
			</div>
			<div class="row">
				<div class="col-md-12">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="single-faq animated fadeInUp mb-5" data-animate="fadeInUp" data-delay=".2" style="visibility: visible; animation-delay: 0.2s;">
                                <h3>How do I choose a VPN?</h3>
                                <p>The most important quality to consider when choosing a VPN service is trust. A trustworthy VPN provider:</p>
                                <ul>
                                    <li>is transparent in its privacy policy</li>
                                    <li>has been independently audited</li>
                                    <li>designs its systems to avoid storing personal data</li>
                                    <li>shows a consistent commitment to internet privacy and digital freedom</li>
                                </ul>
                                <p>After trust, choose your VPN based on the features you need, such as a variety of locations, compatibility with all the devices you own, or 24/7 customer support.</p>
                            </div>
                        </div>
                    </div><!-- .row  -->

                    <div class="row">
                        <div class="col-md-12">
                            <div class="single-faq animated fadeInUp mb-5" data-animate="fadeInUp" data-delay=".2" style="visibility: visible; animation-delay: 0.2s;">
                                <h3>Why should I pay for a VPN?</h3>
                                <p>It’s hard for a free VPN to match the features and service of a quality paid provider, as you might expect. Free VPNs are more likely to deliver a subpar experience, with connections bogged down by a flood of users, no customer service, limited or lax security, and few VPN server locations to choose from.</p>
                                <p>As a paid provider, ExpressVPN can offer connections optimized for speed, security, and stability, as well as a choice of 160 server locations around the world. Plus you can contact our Support Team 24/7 with any questions.</p>
                            </div>
                        </div>
                    </div><!-- .row  -->

                    <div class="row">
                        <div class="col-md-12">
                            <div class="single-faq animated fadeInUp mb-5" data-animate="fadeInUp" data-delay=".2" style="visibility: visible; animation-delay: 0.2s;">
                                <h3>Are VPNs legal?</h3>
                                <p>VPNs are legal and used by individuals and companies around the world to protect their data from snoops and hackers, including in countries with highly restrictive governments. Even where VPNs are seen as discouraged, many governments tacitly endorse their use by officials, academics, or business leaders as necessary to stay competitive in an interconnected world. It is not practical for countries to ban all VPNs.</p>
                                <p>That said, illegal online activity remains illegal, whether you are using a VPN or not.</p>
                            </div>
                        </div>
                    </div><!-- .row  -->

                    <div class="row">
                        <div class="col-md-12">
                            <div class="single-faq animated fadeInUp mb-5" data-animate="fadeInUp" data-delay=".2" style="visibility: visible; animation-delay: 0.2s;">
                                <h3>Can I use a VPN on mobile? Why would I want to?</h3>
                                <p>With apps for iOS and Android, ExpressVPN is easy to use on mobile devices. As more of our digital activity—including banking, streaming, and messaging—happens on our phones and tablets, it is becoming more important to safeguard our data with a VPN on these devices, especially when using public Wi-Fi.</p>
                            </div>
                        </div><!-- .col  -->
                    </div><!-- .row  -->

                    <div class="row">
                        <div class="col-md-12">
                            <div class="single-faq animated fadeInUp" data-animate="fadeInUp" data-delay=".2" style="visibility: visible; animation-delay: 0.2s;">
                                <h3>Will a VPN slow down my internet?</h3>
                                <p>All VPNs have the potential to slow your internet connection, but ExpressVPN’s industry-leading speeds mean users rarely notice a difference. In fact, using a VPN might actually improve your connection if your ISP has been throttling your traffic.</p>
                                <p>In any case, if you do notice that your connection has slowed, make sure you’ve selected the server location that is geographically closest to you.</p>
                            </div>
                        </div><!-- .col  -->
                    </div><!-- .row  -->
				</div><!-- .col -->
			</div><!-- .row -->
		</div><!-- End container -->
	</div>
<?php include 'footer.php';?>
<!--footer section end-->
        <!--Main js file load-->
        <script src="/assets/app/js/main.js"></script>
    </body>
    <script src="/assets/front/js/jquery.bundle.js">
</script>
<script src="/assets/front/js/script.js">
</script>
</html>
