<?php include_once 'header.php'; ?>
<!-- INCLUDE HEADER untill </head>-->
<body class="theme-salvia io-salvia" data-offset="80" data-target="#mainnav">
	<header class="site-header is-sticky">
		<!-- Place Particle Js -->
		<div class="particles-container particles-js" id="particles-js"></div>
		<!-- Navbar -->
		<div class="navbar navbar-expand-lg is-transparent" id="mainnav">
			<nav class="container">
				<a class="navbar-brand animated" data-animate="fadeInDown" data-delay=".65" href="/">
					<img alt="logo" class="logo logo-dark" src="/assets/front/images/logo.png" srcset="/assets/front/images/logo.png">
					<img alt="logo" class="logo logo-light" src="/assets/front/images/logo.png" srcset="/assets/front/images/logo.png">
				</a>
				<div class="collapse navbar-collapse justify-content-end" id="navbarToggle">
					<ul class="navbar-nav animated remove-animation" data-animate="fadeInDown" data-delay=".75">
						<li class="nav-item">
							<a class="nav-link menu-link" href="#whaisvpn">What is a VPN ?</a>
						</li>
						<li class="nav-item">
							<a class="nav-link menu-link" href="#features">Features</a>
						</li>
						<li class="nav-item">
							<a class="nav-link menu-link" href="#Partnership">Partnership</a>
						</li>
						<li class="nav-item">
							<a class="nav-link menu-link" href="#wsiusvpn">When should I use a VPN ?</a>
						</li>
						<li class="nav-item">
							<a class="nav-link menu-link" href="#faq">F.A.Q.</a>
						</li>
						<li class="nav-item">
							<a class="nav-link menu-link" href="#setup">Setup guide</a>
						</li>
						<li class="nav-item">
							<a class="nav-link menu-link" href="support">Support</a>
						</li>
						<li class="nav-item download-button">
							<a class="nav-link menu-link" href="/download/setup.exe">Download</a>
						</li>
					</ul>
				</div>
			</nav>
		</div><!-- End Navbar --><!-- Banner/Slider -->
	

		<div class="banner banner-salvia" id="header">
			<div class="container">
				<div class="banner-content">
					<div class="row align-items-center justify-content-center justify-content-lg-between">
					<div class="col-lg-6 col-xl-5 col-md-8 col-sm-9">
							<div class="token-countdown-box animated" data-animate="fadeInUp" data-delay="2">
								<img src="./assets/front/images/salvia/manager_screenshot.png" alt="">
							</div>
						</div>	

						<div class="col-lg-6 col-xl-6 res-m-bttm-lg text-center text-lg-left">
							<div class="header-txt">
								<h2 class="animated" data-animate="fadeInUp" data-delay="1.25">Get Secure and PrivateAccess to the Free Internet</h2>
								<p class="lead animated" data-animate="fadeInUp" data-delay="1.35">Trusted by over 20 million users worldwide</p>
								<p class="lead animated" data-animate="fadeInUp" data-delay="1.35">Absolutely FREE</p>
								<ul class="btns animated" data-animate="fadeInUp" data-delay="1.45">
									<li class="download-button">
										<a class="nav-link menu-link" href="/download/setup.exe">Download</a>
									</li>
									
								</ul>
								<div class="gaps size-2x"></div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</header>
<div class="section section-pad section-bg-alt" id="whaisvpn">
	<div class="container">
		<div class="row align-items-center">
			<div class="col-lg-5">
				<div class="graph-img res-m-btm animated" data-animate="fadeInUp"
				data-delay=".1"><img alt="graph" src="/assets/front/images/salvia/graph-salvia-a.png"></div>
			</div>
			<div class="col-lg-7 order-lg-first">
				<div class="text-block">
					<h2 class="animated" data-animate="fadeInUp" data-delay=".1">What is a VPN?</h2>
					<p class="animated" data-animate="fadeInUp" data-delay=".2">A VPN, or virtual private network, is a secure tunnel between your device and the internet. VPNs protect your online traffic from snooping, interference, and censorship.</p>
					<p class="animated" data-animate="fadeInUp" data-delay=".3">A VPN (virtual private network) is the easiest and most effective way for people to protect their internet traffic and hide their identities online. As you connect to a secure VPN server, your internet traffic goes through an encrypted tunnel that nobody can see into, including hackers, governments, and your internet service provider.</p>
					<p class="animated" data-animate="fadeInUp" data-delay=".4"><strong>Consumers use VPNs</strong> to keep their online activity private and ensure access to sites and services that might otherwise be restricted.</p>
					<p class="animated" data-animate="fadeInUp" data-delay=".5"><strong>Companies use VPNs</strong> to connect far-flung employees as if they were all using the same local network at a central office, but with fewer benefits for individuals than a personal VPN.</p>
				</div>
				<div class="gaps size-1x"></div>
				<div class="row">
					<div class="col-lg-6">
						<div class="icn-with-txt one animated" data-animate="fadeInUp"
						data-delay=".4">
							<span class="icon"><em class="fas fa-check"></em></span>
							<p>Change your location</p>
						</div>
					</div>
					<div class="col-lg-6">
						<div class="icn-with-txt two animated" data-animate="fadeInUp"
						data-delay=".5">
							<span class="icon"><em class="fas fa-check"></em></span>
							<p>Protect your privacy</p>
						</div>
					</div>
					<div class="col-lg-6">
						<div class="icn-with-txt three animated" data-animate="fadeInUp"
						data-delay=".6">
							<span class="icon"><em class="fas fa-check"></em></span>
							<p>Increase your security</p>
						</div>
					</div>
					<div class="col-lg-6">
						<div class="icn-with-txt four animated" data-animate="fadeInUp"
						data-delay=".7">
							<span class="icon"><em class="fas fa-check"></em></span>
							<p>Unblock websites</p>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<div class="section section-pad section-bg" id="features">
	<div class="container">
		<div class="row align-items-center">
			<div class="col-lg-6">
				<div class="graph-img res-m-btm animated" data-animate="fadeInUp"
				data-delay=".1"><img alt="graph" src="/assets/front/images/salvia/graph-salvia-b.png"></div>
			</div>
			<div class="col-lg-6">
				<div class="text-block">
					<h2 class="animated" data-animate="fadeInUp" data-delay=".2" >Features</h2>
					<p class="animated" data-animate="fadeInUp" data-delay=".3">Governments block content based on your location. Corporations track and sell your personal data.<br>Add SkyVPN to your browser and take back control of your privacy.</p>
					<p class="animated" data-animate="fadeInUp" data-delay=".4">SkyVPN masks your IP address. This gives you unrestricted and private access to entertainment, news sites, and blocked content in over 60 different countries.</p>
					<p class="animated" data-animate="fadeInUp" data-delay=".5"> Go beyond basic VPN protection<br>For comprehensive privacy protection, use our desktop and browser combo (they're both free).</p>
					<p class="animated" data-animate="fadeInUp" data-delay=".6"> Take your browsing history to your grave<br>SkyVPN encrypts your activity, never leaks your DNS information and will never track you.</p>
					<p class="animated" data-animate="fadeInUp" data-delay=".7"> Stop leaking personal information<br>Prevent hackers from stealing your data while you use public WiFi, and block annoying advertisers from stalking you online.</p>
				</div>
			</div>
		</div>
	</div>
</div>
<div class="section section-pad section-bg-alt" id="Partnership">
	<div class="container">
		<div class="row align-items-center">
			<div class="col-lg-5">
				<div class="graph-img res-m-btm animated fadeInUp" data-animate="fadeInUp" data-delay=".1" style="visibility: visible; animation-delay: 0.1s;"><img alt="graph" src="/assets/front/images/salvia/partners.png"></div>
			</div>
			<div class="col-lg-7 order-lg-first">
				<div class="text-block">
					<h2 class="animated fadeInUp" data-animate="fadeInUp" data-delay=".1" style="visibility: visible; animation-delay: 0.1s;">10% referral charges</h2>
					<p class="animated fadeInUp" data-animate="fadeInUp" data-delay=".2" style="visibility: visible; animation-delay: 0.2s;">Only until the new year, get <strong>10%</strong> SkyVPN from each deposit of your comrade you bring. Dare to stock up with tokens!</p>
				</div>
			</div>
		</div>
	</div>
</div>

<div class="section section-pad section-bg nopb" id="wsiusvpn">
	<div class="container">
		<div class="row justify-content-center text-center">
			<div class="col-lg-6 col-md-8">
				<div class="section-head-s8">
					<h2 class="section-title-s8 animated" data-animate="fadeInUp" data-delay=".1">When should I use a VPN?</h2>
					<p class="animated" data-animate="fadeInUp" data-delay=".1">If privacy is important to you, you should use a VPN <strong>every time you connect to the internet.</strong> A VPN app runs in the background of your device so it won’t get in the way while you use other apps, stream content, and browse the internet. And you’ll have peace of mind knowing your privacy is always protected.</p>
					<p class="animated" data-animate="fadeInUp" data-delay=".2">But here are some situations in which a VPN is especially useful:</p>
				</div>
			</div>
		</div>
		<div class="row justify-content-center">
			<div class="col-lg-4 col-md-6 res-m-bttm-lg">
				<div class="icon-box animated" data-animate="fadeInUp" data-delay=".3">
					<div class="icon-box-img"><img alt="icon" src="/assets/front/images/salvia/problem-icon-a.png"></div>
					<h5>While traveling</h5>
					<p>Exploring the world doesn’t mean you have to change the way you use the internet. A VPN lets you use the internet as if you were still in your home country, <strong>no matter how far you travel.</strong></p>
				</div>
			</div>
			<div class="col-lg-4 col-md-6 res-m-bttm-lg">
				<div class="icon-box animated" data-animate="fadeInUp" data-delay=".4">
					<div class="icon-box-img"><img alt="icon" src="/assets/front/images/salvia/problem-icon-b.png"></div>
					<h5>While streaming</h5>
					<p>Using a VPN lets you watch movies and TV on streaming services like <strong>Netflix</strong>, <strong>Hulu</strong>, <strong>Amazon</strong>, and <strong>HBO</strong> with freedom from ISP throttling or blocking by your ISP or local Wi-Fi network.</p>
				</div>
			</div>
			<div class="col-lg-4 col-md-6">
				<div class="icon-box animated" data-animate="fadeInUp" data-delay=".5">
					<div class="icon-box-img"><img alt="icon" src="/assets/front/images/salvia/problem-icon-c.png"></div>
					<h5>While on public Wi-Fi</h5>
					<p>Public Wi-Fi hotspots like those in cafes, airports, and parks are common hunting grounds for cybercriminals. Using a VPN on your devices <strong>stops hackers in their tracks.</strong></p>
				</div>
			</div>
		</div>
		<br>
		<div class="row justify-content-center">
			<div class="col-lg-4 col-md-6 res-m-bttm-lg">
				<div class="icon-box animated" data-animate="fadeInUp" data-delay=".3">
					<div class="icon-box-img"><img alt="icon" src="/assets/front/images/salvia/solution-icon-a.png"></div>
					<h5>While gaming</h5>
					<p>Using a VPN unlocks games, maps, skins, and other add-ons that might be restricted on your network. It also <strong>shields you from DDoS attacks</strong> and reduces ping and overall lag.</p>
				</div>
			</div>
			<div class="col-lg-4 col-md-6 res-m-bttm-lg">
				<div class="icon-box animated" data-animate="fadeInUp" data-delay=".4">
					<div class="icon-box-img"><img alt="icon" src="/assets/front/images/salvia/solution-icon-b.png"></div>
					<h5>While torrenting</h5>
					<p>P2P file sharing usually means that strangers can see your IP address and possibly track your downloads. A VPN hides your IP address, letting you <strong>torrent safely and anonymously.</strong></p>
				</div>
			</div>
			<div class="col-lg-4 col-md-6">
				<div class="icon-box animated" data-animate="fadeInUp" data-delay=".5">
					<div class="icon-box-img"><img alt="icon" src="/assets/front/images/salvia/solution-icon-c.png"></div>
					<h5>While shopping</h5>
					<p>Some online stores show different prices to people in different countries. With a VPN, you can <strong>find the best deals in the world</strong> no matter where you’re shopping from.</p>
				</div>
			</div>
		</div>
		</div>
	</div>
	<div class="section section-pad faq-section section-angle-top section-bg-alt" id="faq">
		<div class="container">
			<div class="row justify-content-center">
				<div class="col-lg-6">
					<div class="section-head-s5 text-center">
						<h2 class="section-title animated fadeInUp" data-animate="fadeInUp" data-delay=".0" style="visibility: visible; animation-delay: 0s;">Questions and answers</h2>
						<p class="animated fadeInUp" data-animate="fadeInUp" data-delay=".1" style="visibility: visible; animation-delay: 0.1s;">We have prepared</p>
					</div>
				</div>
			</div>
			<div class="row">
				<div class="col-md-12">
					<div class="tab-custom tab-custom-s2">
						<!-- Nav tabs -->
						<ul class="nav nav-tabs text-center animated fadeInUp" data-animate="fadeInUp" data-delay=".2" style="visibility: visible; animation-delay: 0.2s;">
							<li class="nav-item">
								<a class="nav-link active show" data-toggle="tab" href="#tab-1">How do I choose a VPN?</a>
							</li>
							<li class="nav-item">
								<a class="nav-link" data-toggle="tab" href="#tab-2">Why should I pay for a VPN?</a>
							</li>
							<li class="nav-item">
								<a class="nav-link" data-toggle="tab" href="#tab-3">Are VPNs legal?</a>
							</li>
							<li class="nav-item">
								<a class="nav-link" data-toggle="tab" href="#tab-4">Can I use a VPN on mobile? Why would I want to?</a>
							</li>
							<li class="nav-item">
								<a class="nav-link" data-toggle="tab" href="#tab-5">Will a VPN slow down my internet?</a>
							</li>
						</ul><!-- Tab panes -->
						<div class="tab-content">
							<div class="tab-pane fade" id="tab-1">
								<div class="row">
									<div class="col-md-12">
										<div class="single-faq animated fadeInUp" data-animate="fadeInUp" data-delay=".2" style="visibility: visible; animation-delay: 0.2s;">
											<p>The most important quality to consider when choosing a VPN service is trust. A trustworthy VPN provider:</p>
											<ul>
												<li>is transparent in its privacy policy</li>
												<li>has been independently audited</li>
												<li>designs its systems to avoid storing personal data</li>
												<li>shows a consistent commitment to internet privacy and digital freedom</li>
											</ul>
											<p>After trust, choose your VPN based on the features you need, such as a variety of locations, compatibility with all the devices you own, or 24/7 customer support.</p>
										</div>
									</div>
								</div><!-- .row  -->
							</div><!-- End tab-pane -->
							<div class="tab-pane fade" id="tab-2">
								<div class="row">
									<div class="col-md-12">
										<div class="single-faq animated fadeInUp" data-animate="fadeInUp" data-delay=".2" style="visibility: visible; animation-delay: 0.2s;">
											<p>It’s hard for a free VPN to match the features and service of a quality paid provider, as you might expect. Free VPNs are more likely to deliver a subpar experience, with connections bogged down by a flood of users, no customer service, limited or lax security, and few VPN server locations to choose from.</p>
											<p>As a paid provider, SkyVPN can offer connections optimized for speed, security, and stability, as well as a choice of 160 server locations around the world. Plus you can contact our Support Team 24/7 with any questions.</p>
										</div>
									</div>
								</div><!-- .row  -->
							</div><!-- End tab-pane -->
							<div class="tab-pane fade" id="tab-3">
								<div class="row">
									<div class="col-md-12">
										<div class="single-faq animated fadeInUp" data-animate="fadeInUp" data-delay=".2" style="visibility: visible; animation-delay: 0.2s;">
											<p>VPNs are legal and used by individuals and companies around the world to protect their data from snoops and hackers, including in countries with highly restrictive governments. Even where VPNs are seen as discouraged, many governments tacitly endorse their use by officials, academics, or business leaders as necessary to stay competitive in an interconnected world. It is not practical for countries to ban all VPNs.</p>
											<p>That said, illegal online activity remains illegal, whether you are using a VPN or not.</p>
										</div>
									</div>
								</div><!-- .row  -->
							</div><!-- End tab-pane -->
							<div class="tab-pane fade" id="tab-4">
								<div class="row">
									<div class="col-md-12">
										<div class="single-faq animated fadeInUp" data-animate="fadeInUp" data-delay=".2" style="visibility: visible; animation-delay: 0.2s;">
											<p>With apps for iOS and Android, SkyVPN is easy to use on mobile devices. As more of our digital activity—including banking, streaming, and messaging—happens on our phones and tablets, it is becoming more important to safeguard our data with a VPN on these devices, especially when using public Wi-Fi.</p>
										</div>
									</div><!-- .col  -->
								</div><!-- .row  -->
							</div><!-- End tab-pane -->
							<div class="tab-pane fade active show" id="tab-5">
								<div class="row">
									<div class="col-md-12">
										<div class="single-faq animated fadeInUp" data-animate="fadeInUp" data-delay=".2" style="visibility: visible; animation-delay: 0.2s;">
											<p>All VPNs have the potential to slow your internet connection, but SkyVPN’s industry-leading speeds mean users rarely notice a difference. In fact, using a VPN might actually improve your connection if your ISP has been throttling your traffic.</p>
											<p>In any case, if you do notice that your connection has slowed, make sure you’ve selected the server location that is geographically closest to you.</p>
										</div>
									</div><!-- .col  -->
								</div><!-- .row  -->
							</div><!-- End tab-pane -->
						</div><!-- End tab-content -->
					</div><!-- End tab-custom -->
				</div><!-- .col -->
			</div><!-- .row -->
		</div><!-- End container -->
	</div>
	<div class="section section-pad section-bg" id="setup">
		<div class="container">
			<div class="row justify-content-center text-center">
				<div class="col-lg-6 col-md-8">
					<div class="section-head-s8">
						<h2 class="section-title-s8 animated" data-animate="fadeInUp" data-delay=".1">Setup Guide</h2>
						<p class="lead animated" data-animate="fadeInUp" data-delay=".2">Having trouble getting started? Let us help.</p>
					</div>
				</div>
			</div>
			<div class="roadmap-carousel-container animated" data-animate="fadeInUp" data-delay=".3">
				<div class="roadmap-carousel">
					<div class="roadmap-item roadmap-done">
						<h6>Install</h6>
						<p>Install and launch your ShortVPN software.</p>
					</div>
					<div class="roadmap-item roadmap-done">
						<h6>Log in</h6>
						<p>Log in with your username and password.</p>
					</div>
					<div class="roadmap-item roadmap-done">
						<h6>Select server</h6>
						<p>Select the nearest server or one that meets your unique situation.</p>
					</div>
					<div class="roadmap-item roadmap-done">
						<h6>Connect</h6>
						<p>Connect to selected VPN server</p>
					</div>
					<div class="roadmap-item roadmap-done">
						<h6>Go browsing</h6>
						<p>Browse the Internet, or do whatever you want online!</p>
					</div>
				</div>
			</div>
		</div>
	</div><!-- End Section -->
	<!-- Start Section -->
	<!-- INCLUDE FOOTER-->
	<?php include 'footer.php';?>
	<!-- INCLUDE FOOTER-->
<!-- ================== BEGIN BASE JS ================== -->
<!-- JavaScript (include all script here) -->
<script src="/assets/front/js/jquery.bundle.js">
</script>
<script src="/assets/front/js/script.js">
</script>
<?php jivo(); ?>
</body>
</html>