<!DOCTYPE html>
<html lang="en" class="js">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="description" content="SKYVPN Best VPN for any devices">
        <meta name="keywords" content="SKYVPN, best vpn, vpn service, fastest vpn, anonymus service, anonymus vpn, bypass browser lock, bypass site lock, hide ip, change ip,">
        <title>SKYVPN</title>
        <!--Favicon add-->
        <link rel="shortcut icon" type="image/png" href="/assets/images/logo/icon.png">
        <!--Style Css-->
        <link href="css/second.css" rel="stylesheet">

        <link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/sweetalert/1.1.3/sweetalert.min.css">
        <link href="/assets/front/css/vendor.bundle.css" rel="stylesheet">
        <link href="https://fonts.googleapis.com/css?family=Roboto:100,100italic,300,300italic,400,400italic,500,500italic,700,700italic,900,900italic" rel="stylesheet" type="text/css" />
        <!--Font Awasome-->
        <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.4.2/css/all.css" integrity="sha384-/rXc/GQVaYpyDdyxK+ecHPVYJSN9bmVFBvjA/9eOB+pb3F2w2N6fc5qB9Ew5yIns" crossorigin="anonymous">
        <link rel="stylesheet" href="../assets/front/css/FontAwasomeCSS/font-awesome.css">
        <!--SCRIPTS-->

        <script src="/assets/admin/global/plugins/jquery.min.js" type="text/javascript"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/sweetalert/1.1.3/sweetalert.min.js"></script>
        <script type="text/javascript" src="/assets/user/js/functions.js"></script>
        <script type="text/javascript" src="/assets/front/js/script.js"></script>
    </head>
    <body>
    <header class="site-header">
        <!--navbar area start-->
        <div class="navbar navbar-expand-lg is-transparent" id="mainnav">
			<nav class="container">
				<a class="navbar-brand animated" data-animate="fadeInDown" data-delay=".65" href="/">
					<img alt="logo" class="logo" src="/assets/front/images/logo.png">
				</a>
				<div class="collapse navbar-collapse justify-content-end" id="navbarToggle">
					<ul class="navbar-nav animated remove-animation" data-animate="fadeInDown" data-delay=".75">
						<li class="nav-item">
							<a class="nav-link menu-link" href="features">Features</a>
						</li>
						<li class="nav-item">
							<a class="nav-link menu-link" href="support">Support</a>
						</li>
						<li class="nav-item download-button">
							<a class="nav-link menu-link btn" href="/download/setup.exe">Download</a>
						</li>
					</ul>
				</div>
			</nav>
		</div>
    </header>

    <div class="section section-pad section-bg nopb" id="overview">
	<div class="container">
        <div class="row justify-content-center text-center mb-100">
			<div class="col-lg-6 col-md-8">
				<div class="section-head-s8">
					<h1 class="section-title-s8 animated" data-animate="fadeInUp" data-delay=".1">Features</h1>
				</div>
			</div>
		</div>
		<div class="row align-items-center mb-100">
			<div class="col-lg-6">
				<div class="graph-img res-m-btm animated" data-animate="fadeInUp"
				data-delay=".1"><img alt="graph" src="/assets/front/images/salvia/sample-840x500.png"></div>
			</div>
			<div class="col-lg-6">
                <div class="text-block">
                    <h2 class="section-title animated" data-animate="fadeInUp" data-delay=".2" >Access anything & everything</h2>
					<p class="animated" data-animate="fadeInUp" data-delay=".3">Picture an online universe of infinite possibilities. A place where you could unblock websites, enjoy your favorite content, or stream sports events without any limitations. A world where you didn’t have to worry about losing access to your favorite TV series when traveling abroad. CyberGhost is the best VPN for streaming and bypassing geo-restrictions that makes all this a reality.</p>
				</div>
			</div>
        </div>

        <div class="row align-items-center mb-100">
			
			<div class="col-lg-6">
                <div class="text-block">
                    <h2 class="section-title animated" data-animate="fadeInUp" data-delay=".2" >Online anonymity & safety for your data</h2>
					<p class="animated" data-animate="fadeInUp" data-delay=".3">Without a VPN service, everything you do online gets tracked, followed, and even spied on. However, when using a virtual private network, you’re virtually putting on an online invisibility cloak. Everything you do behind this encrypted connection is 100% anonymous and untraceable. Thus, your private data is protected, and you’re safer online.</p>
				</div>
            </div>
            
            <div class="col-lg-6">
				<div class="graph-img res-m-btm animated" data-animate="fadeInUp"
				data-delay=".1"><img alt="graph" src="/assets/front/images/salvia/sample-840x500.png"></div>
			</div>
        </div>

        <div class="row align-items-center mb-100">
			<div class="col-lg-6">
				<div class="graph-img res-m-btm animated" data-animate="fadeInUp"
				data-delay=".1"><img alt="graph" src="/assets/front/images/salvia/sample-840x500.png"></div>
			</div>
			<div class="col-lg-6">
                <div class="text-block">
                    <h2 class="section-title animated" data-animate="fadeInUp" data-delay=".2" >Absolute privacy on all devices</h2>
					<p class="animated" data-animate="fadeInUp" data-delay=".3">Online data safety does not stop at one device. Yet we often lose track of all the smart gear that’s always connected to the internet. All of it needs VPN protection. With CyberGhost VPN, you can enjoy a secure VPN connection on up to 7 devices simultaneously. Our state-of-the-art VPN security, along with our strict no-logs policy, guarantee total data anonymity across all apps and platforms.</p>
				</div>
			</div>
        </div>

        <div class="row align-items-center mb-100">
			
			<div class="col-lg-6">
                <div class="text-block">
                    <h2 class="section-title animated" data-animate="fadeInUp" data-delay=".2" >Hide your IP and mask your location online</h2>
					<p class="animated" data-animate="fadeInUp" data-delay=".3">Disguise your whereabouts and surf anonymously. When you connect to any ExpressVPN server in 94 countries around the world, your IP address and location are hidden. Foil companies that try to raise prices based on where you are, and mask your true IP address while torrenting with a VPN.</p>
				</div>
            </div>
            
            <div class="col-lg-6">
				<div class="graph-img res-m-btm animated" data-animate="fadeInUp"
				data-delay=".1"><img alt="graph" src="/assets/front/images/salvia/sample-840x500.png"></div>
			</div>
        </div>

        <div class="row align-items-center">
			<div class="col-lg-6">
				<div class="graph-img res-m-btm animated" data-animate="fadeInUp"
				data-delay=".1"><img alt="graph" src="/assets/front/images/salvia/sample-840x500.png"></div>
			</div>
			<div class="col-lg-6">
                <div class="text-block">
                    <h2 class="section-title animated" data-animate="fadeInUp" data-delay=".2" >IP addresses and URLs</h2>
					<p class="animated" data-animate="fadeInUp" data-delay=".3">Every machine on the internet is identified by a string of numbers called an IP address. Your computer has an IP address, and so do all other computers, phones, servers, and networked devices. Some IP addresses are fixed, and some change periodically, and they are difficult for humans to remember.</p>
					<p class="animated" data-animate="fadeInUp" data-delay=".3">That’s why IP addresses of websites need to be translated into words and phrases better suited for humans. We call these words URLs (Uniform Resource Locators).</p>
				</div>
			</div>
        </div>
        
	</div>
</div>
<?php include 'footer.php';?>
<!--footer section end-->
        <!--Main js file load-->
        <script src="/assets/app/js/main.js"></script>
    </body>
    
<script src="/assets/front/js/jquery.bundle.js">
</script>
<script src="/assets/front/js/script.js">
</script>
</html>
